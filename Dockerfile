ARG ARGO_CD_VERSION="v2.11.2"
ARG KSOPS_VERSION="v4.3.1"
ARG VAULT_VERSION="1.16.3"

#--------------------------------------------#
#--------Get KSOPS and Kustomize-----------#
#--------------------------------------------#

FROM docker.io/viaductoss/ksops:$KSOPS_VERSION as ksops-builder

#--------------------------------------------#
#---------------Get Vault--------------------#
#--------------------------------------------#

FROM docker.io/alpine:3.20.0 as vault-builder
ARG VAULT_VERSION
RUN wget -qO- https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip \
    |  unzip -p - vault > /tmp/vault && chmod +x /tmp/vault 

#--------------------------------------------#
#--------Build Custom Argo Image-------------#
#--------------------------------------------#

FROM quay.io/argoproj/argocd:$ARGO_CD_VERSION

# Switch to root for the ability to perform install
USER root

ARG PKG_NAME=ksops

# Override the default kustomize executable with the Go built version
COPY --from=ksops-builder /go/bin/kustomize /usr/local/bin/kustomize.real

# Add ksops executable to path
COPY --from=ksops-builder /go/bin/ksops /usr/local/bin/ksops

COPY --from=vault-builder /tmp/vault /usr/local/bin/vault

COPY ./kustomize /usr/local/bin

# Switch back to non-root user
USER 999
